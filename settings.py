# coding: utf-8

import os
import yaml

from tornado.options import define, options

# root directory
ROOT = os.path.dirname(os.path.abspath(__file__))

# logging files directory
LOG_DIR = os.path.join(ROOT, 'logs')

# configuration files directory
CONF_DIR = os.path.join(ROOT, "config")
db_conf_file = os.path.join(CONF_DIR, "database.yaml")
env_conf_file = os.path.join(CONF_DIR, "environ.yaml")

# database config
with open(db_conf_file) as stream:
    db = yaml.load(stream)["databases"]

define("port", default=8001, help="run on the given port", type=int)
define("debug", default=False, help="debug mode", type=bool)
define("address", default='0.0.0.0', help="run on given address")
define("process", default=0, help='set up number of process', type=int)
options.parse_command_line()
settings = {}

# environment variables
with open(env_conf_file) as stream:
    env = yaml.load(stream)
    settings['autoreload'] = env["autoreload"]
    settings['cookie_secret'] = env["cookie_secret"]
    settings['debug'] = options.debug
    settings['logging'] = env["logging"]
    options.debug = env["debug"]
    options.process = env["process"]

