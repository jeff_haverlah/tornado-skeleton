# coding: utf-8


import tornado.escape
import tornado.web
import traceback


class RequestHandler(tornado.web.RequestHandler):

    def write_error(self, status_code, **kwargs):
        self.set_header("Content-Type", "text/json")
        if self.settings.get("serve_traceback") and "exc_info" in kwargs:
            lines = []
            for line in traceback.format_exception(*kwargs["exc_info"]):
                lines.append(line)
            self.finish({"error": {
                             "code": status_code,
                             "message": self._reason,
                             "traceback": lines,
                             }
                       })
        else:
            self.finish({"error": {
                             "code": status_code,
                             "message": self._reason,
                             }
                       })

    def __delist_arguments(self, args):
        for key, value in args.items():
            if len(value) == 1:
                args[key] = value[0].decode("utf-8")
        return args

    def get_url_args(self):
        return self.__delist_arguments(self.request.query_arguments)

    def get_body_args(self):
        return self.__delist_arguments(self.request.body_arguments)

    def get_json_args(self):
        return tornado.escape.json_decode(self.request.body)
