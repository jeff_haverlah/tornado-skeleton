# coding: utf-8

import tornado.gen

from app.handlers.request import RequestHandler
from app.utils.errors import ApplicationError


class ApiHandler(RequestHandler):

    @tornado.gen.coroutine
    def get(self, endpoint=None):
        if endpoint:
            response = {"msg": "GET request rendered {}".format(endpoint)}
        else:
            response = {"msg": "GET request without endpoint!"}
        self.finish(response)

    @tornado.gen.coroutine
    def post(self):
        self.finish({"msg": "POST request"})
