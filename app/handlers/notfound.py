# coding: utf-8

import tornado.gen

from app.handlers.request import RequestHandler
from app.utils.errors import ApplicationError

class NotFoundHandler(RequestHandler):

    @tornado.gen.coroutine
    def get(self):
        raise ApplicationError(404, reason=u"Page not found")

    @tornado.gen.coroutine
    def post(self):
        raise ApplicationError(405, reason=u"Method not allowed")

    @tornado.gen.coroutine
    def patch(self):
        raise ApplicationError(405, reason=u"Method not allowed")

    @tornado.gen.coroutine
    def put(self):
        raise ApplicationError(405, reason=u"Method not allowed")

    @tornado.gen.coroutine
    def delete(self):
        raise ApplicationError(405, reason=u"Method not allowed")