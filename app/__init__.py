# coding: utf-8

import tornado.web

from app.utils.logger import log
from app.handlers import NotFoundHandler, ApiHandler
from settings import db, options, settings

class Application(tornado.web.Application):

    def __init__(self):
        handlers = (
                (r"/api/?", ApiHandler),
                (r"/api/([0-9]+)/?", ApiHandler),
                (r"/.*", NotFoundHandler),
        )

        tornado.web.Application.__init__(self, handlers, **settings)

        #Model.set_connection_resolver(DatabaseManager(db))



def main():
    import tornado.process
    from tornado.netutil import bind_sockets
    from tornado.httpserver import HTTPServer
    sockets = bind_sockets(options.port, options.address)
    process_id = tornado.process.fork_processes(options.process)
    log.info("Server running at  http://{0}:{1}".format(
        options.address, options.port))
    app = Application()
    server = HTTPServer(app, xheaders=True)
    server.add_sockets(sockets)
    io_loop = tornado.ioloop.IOLoop.current()
    try:
        io_loop.start()
    except KeyboardInterrupt:
        log.info("Stopping server.")
        io_loop.stop()
