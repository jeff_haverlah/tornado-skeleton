# coding: utf-8

import tornado.web


class ApplicationError(tornado.web.HTTPError):
    pass
