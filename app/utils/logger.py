#coding: utf-8

from settings import LOG_DIR
import logging

formatter = logging.Formatter(fmt='%(asctime)s [%(levelname)5s]  %(message)s', datefmt='%d.%m.%Y %H:%M:%S')

handler = logging.handlers.TimedRotatingFileHandler(LOG_DIR + '/info.log', when='W0', backupCount=26)
handler.setFormatter(formatter)
log = logging.Logger('info')
log.setLevel(logging.INFO)
log.addHandler(handler)
