# README #

### Set Up ###
 
 **Using pip and virtual environment**

* Activate a virtual environment

```bash
$ virtualenv .venv
$ source .venv/bin/activate
```

* Installing dependencies:

```bash
$ pip install -r requirements.txt
```